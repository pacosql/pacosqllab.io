# Pacos QL

## Setting credentials

If you have permissions to push to this repository, the first thing to do is to setup the local credentials, as follow:

```
git config --local user.name "Pacos QL"
git config --local user.email "pacosql@protonmail.com"
```

Every commit must be associated to this anonymous user, be careful with that.

## About the project
This project was built using [next.js](https://nextjs.org) and you're free and encouraged to extend this by sending PRs.

## Running the project
To run this project locally, you need to have `yarn` or `npm` installed, then you 

``` 
yarn dev (npm run dev)
```

This will create a local server running on `http://localhost:3000`.
