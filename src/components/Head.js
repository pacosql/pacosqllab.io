import React from 'react';
import HeadDom from 'next/head'

const Head = (props) => (
	<HeadDom>
		<title>Pacos QL</title>
		<meta name="description" content="Manda mensajes a tus pacos" />
		<meta name="viewport" content="width=device-width  initial-scale=1,shrink-to-fit=no" />
	</HeadDom>
)


export default Head;
