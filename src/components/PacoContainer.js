import React from 'react'

import pacoList from '../pacos.json'

const PacoContainer = () => {
	const randomIndex = Math.floor(Math.random() * pacoList.length)

	const randomPaco = pacoList[randomIndex]

	const cleanPhoneNumber = (number) => "56" + number.toString().replace(/\-/g, '')

	return (
		<React.Fragment>
			<section className="paco-data">
				<h2>{randomPaco.NOMBRE}</h2>
				<p><strong>Grado:</strong> {randomPaco.GRADO_DESCRIPCION}</p>
				<p><strong>Correo:</strong> {randomPaco.EMAIL1}, {randomPaco.EMAIL2}</p>
				<p><strong>Teléfono:</strong> {randomPaco.TEL_PER}</p>
			</section>

			<section className="send-buttons">
				<a href={`https://wa.me/${cleanPhoneNumber(randomPaco.TEL_PER)}&text=PACO%20QL`}>Manda Paco culiao por WhatsApp</a>
			</section>
		</React.Fragment>
	)
}

export default PacoContainer;
