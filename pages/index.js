import Head from '../src/components/Head'
import PacoContainer from '../src/components/PacoContainer'

const Index = () => (
	<div>
		<Head />
		<PacoContainer />
	</div>
);

export default Index;

